// variables
const carrito = document.getElementById('carrito');
const cursos = document.getElementById('lista-cursos');
const listaCursos = document.querySelector('#lista-carrito tbody');
const vaciarCarritoBtn = document.getElementById('vaciar-carrito');

// listeners
cargarEventListeners();

function cargarEventListeners () {
    // dispara cuando se presiona "agregar carrito"
    cursos.addEventListener('click', comprarCurso);

    // cuando se elimina un curso del carrito
    carrito.addEventListener('click', eliminarCurso);

    // al vaciar el carrito
    vaciarCarritoBtn.addEventListener('click', vaciarCarrito);

    // al cargar el documento, mostrar Local Storage
    document.addEventListener('DOMContentLoaded', leerLocalStorage);
}

// funciones

// funcion que añade el curso al carrito
function comprarCurso (e) {
    e.preventDefault();
    // Delegation para agregar carrito
    if (e.target.classList.contains('agregar-carrito')) {
        const curso = e.target.parentElement.parentElement;
        // enviamos el curso seleccionado para tomar sus datos
        leerDatosCurso(curso);
    }
}

// lee los datos del curso
function leerDatosCurso (curso) {
    const infoCurso = {
        imagen: curso.querySelector('img').src,
        titulo: curso.querySelector('h4').textContent,
        precio: curso.querySelector('.precio span').textContent,
        id: curso.querySelector('a').getAttribute('data-id')
    }

    // validar si ya se ha agregado el producto al carrito
    if (validarProductoCarro(infoCurso) === false) {
        insertCarrito(infoCurso);
        
        guardarCursoLocalSorage(infoCurso);
    }
}

// muestra el curso seleccionado en el carrito
function insertCarrito (curso) {
    const row = document.createElement('tr');
    row.innerHTML = `
        <td><img src="${curso.imagen}" width="100" /></td>
        <td>${curso.titulo}</td>
        <td>${curso.precio}</td>
        <td>
            <a href="#" class="borrar-curso" data-id="${curso.id}">X</a>
        </td>
    `;

    listaCursos.appendChild(row);
}

// elimina el curso del carrito en el dom
function eliminarCurso (e) {
    e.preventDefault();
    let curso, cursoId;

    if (e.target.classList.contains('borrar-curso')) {
        e.target.parentElement.parentElement.remove();
        curso = e.target.parentElement.parentElement;
        cursoId = curso.querySelector('a').getAttribute('data-id');
    }

    eliminarCursoLocalStorage(cursoId);
}

// eliminar los cursos del carrito en el DOM
function vaciarCarrito () {
    while (listaCursos.firstChild) {
        listaCursos.removeChild(listaCursos.firstChild);
    }

    // vaciar local storage
    vaciarLocalStorage();

    return false;
}

// almacena cursos en el carrito a Local Storage
function guardarCursoLocalSorage (curso) {
    let cursos;
    // toma el valor de un arreglo con datos de LS o vacio
    cursos = obtenerCursosLocalStorage();

    // el curso seleccionado se agrega al arreglo
    cursos.push(curso);

    // JSON.stringify, convierte un array en string
    localStorage.setItem('cursos', JSON.stringify(cursos));
}

// comprueba que haya elementos en Local Storage
function obtenerCursosLocalStorage () {
    let cursosLS;

    // comprobamos si hay algo en Local Storage
    if (localStorage.getItem('cursos') === null) {
        cursosLS = [];
    } else {
        // JSON.parse, hace que lo que viene como string, lo comvierta a array
        cursosLS = JSON.parse(localStorage.getItem('cursos'));
    }

    return cursosLS;
}

// imprime los cursos del local Storage en el carrito
function leerLocalStorage () {
    let cursosLS;

    cursosLS = obtenerCursosLocalStorage();

    cursosLS.forEach((curso) => {
        // construir el template
        insertCarrito(curso);
    });
}

// validamos si el producto ya fue registrado previamente en el carrito
function validarProductoCarro (curso) {
    let cursosLS, existe = false;

    cursosLS = obtenerCursosLocalStorage();

    cursosLS.forEach((infoCurso) => {
        if (infoCurso.id === curso.id) {
            existe = true;
        }
    });

    return existe;
}

// elimina el curso por el Id en localstorage
function eliminarCursoLocalStorage (cursoId) {
    let cursosLS;

    cursosLS = obtenerCursosLocalStorage();

    cursosLS.forEach((curso, index) => {
        if (curso.id === cursoId) {
            cursosLS.splice(index, 1);
        }
    });

    localStorage.setItem('cursos', JSON.stringify(cursosLS));
}

// elimina todos los cursos del local storage
function vaciarLocalStorage () {
    localStorage.clear();
}